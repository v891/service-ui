import React, {useState} from 'react';
import './App.css';
import {AsyncTypeahead} from "react-bootstrap-typeahead";

const SEARCH_URI = 'http://localhost:8080/api/search';
const filterBy = () => true;

interface SearchItem {
    id: string;
    name: string;
}


function App() {
    const [isLoading, setIsLoading] = useState(false);
    const [options, setOptions] = useState<SearchItem[]>([]);
    const handleSearch = (query: string) => {
        setIsLoading(true);

        fetch(`${SEARCH_URI}/${query}`)
            .then((resp) => resp.json())
            .then((items: SearchItem[]) => {
                console.log(items)
                setOptions(items);
                setIsLoading(false);
            })
            .catch((error) => {
                console.error(error);
            });
    };
    return (
        <div className="App">
            {/*<header className="App-header">*/}
            <AsyncTypeahead className="Typeahead"
                            filterBy={filterBy}
                            id="async-example"
                            isLoading={isLoading}
                            labelKey="name"
                            minLength={3}
                            onSearch={handleSearch}
                            options={options}
                            placeholder="Search for a Github user..."
                            renderMenuItemChildren={(option: any) => (
                                <>
                                    <span>{option.name}</span>
                                </>
                            )}
                            inputProps={{className: 'Typeahead-input'}}
            />
        </div>
    );
}

export default App;
